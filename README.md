# redis_client

Проект для работы с redis

## Установка

1. Создайте папку для проекта:
```
mkdir redis
cd redis
```
2. Склонируйте репозиторий: 
```
git clone https://gitlab.com/VBolshakovsky/redis.git
```
3. Создаем виртуальное окружение
Windows:
```
python -m venv venv
```
Linux:
```
python3 -m venv venv
```
3. Активируем виртуальное окружение
Windows:
```
venv\Scripts\Activate.ps1
```
Linux:
```
source env/bin/activate
```
3. Обновим pip
```
python -m pip install --upgrade pip
```
4. Установите необходимые пакеты: 
```
pip install -r .\redis\requirements\base.txt
```
5. Запустите redis в докере
```
docker compose -f "redis\docker-compose.yml" up -d --build
```

## Использование

redis\src\service\redis\example.py

## Лицензия

No license file
