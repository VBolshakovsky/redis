"""
Description: Working with Redis
Python version: 3.11.2
Author: Bolshakov
Creation date: 11.05.2023
Links:
    https://pythonru.com/biblioteki/redis-python
    https://github.com/redis/redis
    Команды
    https://redis-py.readthedocs.io/en/stable/commands.html
    RedisJSON
    https://developer.redis.com/howtos/redisjson/using-python/
    https://pypi.org/project/rejson/
Comment:
    #При наименовании ключей, придерживаться схемы. "object-type:id"
    #например: "user:1000", «comment:4321:reply.to» или «comment:4321:reply-to».

    #.set_m_hash лучше не использовать, устаревший метод:
    # DeprecationWarning: Redis.hmset() is deprecated. Use Redis.hset() instead.
    
    Не называй файлик с redis, redis.py
"""

import json
from typing import Union
import redis


class RedisClientError(Exception):
    """
    ### Пользовательский класс для работы с ошибками
    """

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return f"Ошибка клиента Redis. {self.msg}"


class RedisClient:
    """ 
    ### Класс для подключения и работы с Redis

    Args:
    - host            : имя сервера
    - port            : порт(по умолчанию 6379)
    - db              : поддерживает несколько баз данных 
                        в одном экземпляре(от 0 до 15)(по умолчанию 0)
    - password        : пароль
    - connection_pool : доп парамметры
    """

    def __init__(self,
                 host: str = '127.0.0.1',
                 port: int = 6379,
                 db: int = 0,
                 password: str = None,
                 connection_pool: dict = None):
        self.host = host
        self.port = port
        self.db = db
        self.password = password
        self.connection_pool = connection_pool
        self.r = None
        self.__connect()

    def __connect(self) -> None:
        """
        ### Метод приватный для создания подключения
        decode_responses=True - декодирует полученные бинарные данные
        """
        self.r = redis.StrictRedis(host=self.host,
                                   port=self.port,
                                   password=self.password,
                                   db=self.db,
                                   charset="utf-8",
                                   decode_responses=True,
                                   connection_pool=self.connection_pool)

    def set(self,
            key: str,
            value,
            ex: int = None,
            nx: bool = False,
            xx: bool = False) -> bool:
        """
        ### Метод для установки значения(type:string)
        
        Args:
        - ex    : параметр, определяющий время жизни ключа (в секундах).
        - nx    : флаг, установка значения только в случае, если ключ отсутствует в Redis.
        - xx    : флаг, установка значения только в случае, если ключ имеет уже установленное значение.
        
        Out:
        - Успешность(True/False)
        """

        return self.r.set(key, value, ex=ex, nx=nx, xx=xx)

    def get(self, key: str) -> Union[str, None]:
        """
        ### Метод для получения значения, по ключу(type:string)
        
        Args:
        - key   : имя ключа
        
        Out:
        - Возвращает значение по имени ключа или None, если ключ не существует
        """

        return self.r.get(key)

    def set_m_hash(self, key: str, mapping: dict) -> bool:
        """
        ### Метод для установки значения хэш-множества(словарь), по ключу
        .hmset("user:"', {"key1": "value1", "key2": "value2"})
        
        Args:
        - key     : имя ключа
        - mapping : множество в виде словаря
        
        Out:
        - Успешность(True/False)
        """

        return self.r.hmset(key, mapping)

    def set_hash(self, key: str, value: str, mapping: any) -> int:
        """
        ### Метод для установки хэша, по ключу
        .hset("user:1", "name", "John")
        
        Args:
        - key     : имя ключа
        - value   : значение
        - mapping : множество в виде словаря
        
        Out:
        - Возвращает количество добавленных полей
        """

        return self.r.hset(key, value, mapping)

    def get_hash_all(self, key: str) -> Union[dict, None]:
        """
        ### Метод для получения всех значений хэш-множества, по ключу
        .hgetall("user:1")
        
        Args:
        - key     : имя ключа
        
        Out:
        - Возвращает словарь значений
        """

        return self.r.hgetall(key)

    def get_hash(self, key: str, value: str) -> Union[str, None]:
        """
        ### Метод для получения значения хэша, по ключу и значению
        .hget("user:1", "name")
        
        Args:
        - key     : имя ключа
        - value   : значение
        
        Out:
        - Возвращает значение ключа в имени хеша
        """

        return self.r.hget(key, value)

    def get_hash_exists(self, key: str, value: str) -> bool:
        """
        ### Метод для проверки наличия ключа в хэш-множестве, по ключу и значению
        .hexists("user:1", "name")
        
        Args:
        - key     : имя ключа
        - value   : значение
        
        Out:
        - Возвращает логическое значение, указывающее, существует ли ключ в имени хеша.
        """

        return self.r.hexists(key, value)

    def del_hash(self, key: str, value: str) -> int:
        """
        ### Метод для удаления в хэш-множестве, по ключу и значению
        .hdel("user:1", "name")
        
        Args:
        - key     : имя ключа
        - value   : значение
        
        Out:
        - Возвращает количество удаленных полей
        """

        return self.r.hdel(key, value)

    def set_list(self, key: str, *values: str):
        """
        ### Метод для добавления элементов в список, по ключу
        .rpush('my_list', 'value1', 'value2', 'value3')
        
        Args:
        - key     : имя ключа
        - values  : значения
        
        Out:
        - Возвращает количество добавленных значений
        """

        return self.r.rpush(key, *values)

    def get_list(self, key: str) -> list:
        """
        ### Метод для получения элементов списка, по ключу
        
        Args:
        - key     : имя ключа
        
        Out:
        - Возвращает элементы списка
        """

        return self.r.lrange(key, 0, -1)

    def get_list_index(self, key: str, index: int) -> Union[str, None]:
        """
        ### Метод для получения элемента списка, по ключу и индексу
        .lindex('my_list', 0)
        
        Args:
        - key     : имя ключа
        - index   : индекс
        Out:
        - Возвращает элемент списка
        """

        return self.r.lindex(key, index)

    def set_list_index(self, key: str, index: int, value) -> bool:
        """
        ### Метод для изменения элемента списка, по ключу и индексу
        .lset('my_list', 1, 'orange')
        
        Args:
        - key     : имя ключа
        - index   : индекс
        
        Out:
        - Успешность(True/False)
        """

        return self.r.lset(key, index, value)

    def del_list(self, key: str, count: int, value: str) -> int:
        """
        ### Метод для удаления count эллементов из списка, по ключу и значению
        count > 0: удалить элементы, равные значению, перемещаясь от начала к концу.
        count < 0: удалить элементы, равные значению, перемещаясь от конца к началу.
        count = 0: удалить все элементы, равные значению.
        .lrem('my_list', 0, 'orange')
        
        Args:
        - key     : имя ключа
        - index   : индекс
        
        Out:
        - Возвращает количество удаленных значений
        """

        return self.r.lrem(key, count, value)

    def search_list(self, key: str, value: any) -> Union[list, None]:
        """
        ### Метод для поиска элементов списка, по ключу и значению
        
        Args:
        - key     : имя ключа
        - value   : значение
        
        Out:
        - Возвращает найденные значений
        """

        elements = self.r.lrange(key, 0, -1)
        result = []
        for i, element in enumerate(elements):
            if element == value:
                result.append(i)
        if result:
            return result

        return None

    def set_json(self, key: str, data: dict) -> bool:
        """
        ### Метод для сохранения данных в json, по ключу
        На самом деле сохраняется в string, для полноценной работы с json
        Redis должен иметь модуль RedisJSON
        
        Args:
        - key    : имя ключа
        - data   : json
        
        Out:
        - Успешность(True/False)
        """

        return self.r.set(key, json.dumps(data))

    def get_json(self, key: str) -> Union[dict, None]:
        """
        ### Метод для получения данных в json, по ключу
        
        Args:
        - key    : имя ключа
        
        Out:
        - Возаращает json
        """

        data = self.r.get(key)
        #Проверим на формат json
        try:
            return json.loads(data)
        except:
            return None

    def type(self, key: str) -> str:
        """
        ### Метод для получения типа данных Redis, по ключу
        string, list, set, zset, hash and stream
        
        Args:
        - key    : имя ключа
        
        Out:
        - Возаращает один из форматов
        string, list, set, zset, hash and stream
        """

        return self.r.type(key)

    def expire(self,
               key: str,
               time: int,
               nx=False,
               xx=False,
               gt=False,
               lt=False) -> bool:
        """
        ### Метод для установки срока действия, по ключу

        Args:
        - time  : Параметр, определяющий время жизни ключа (в секундах), может быть Python.timedelta.
        - nx    : Флаг, установка значения только в случае, если ключ отсутствует в Redis.
        - xx    : Флаг, установка значения только в случае, если ключ имеет уже установленное значение.
        - gt    : Флаг, установка значения только в случае, когда новый срок действия больше текущего.
        - lt    : Флаг, установка значения только в случае, когда новый срок действия меньше текущего.
        
        Out:
        - Успешность(True/False)
        """

        return self.r.expire(key, time, nx=nx, xx=xx, gt=gt, lt=lt)

    def delete(self, *key: str) -> int:
        """
        ### Метод для удаления ключа, по ключу
        .delete('key1', 'key2', 'key3')
        
        Args:
        - key     : имя ключа/ключей
        
        Out:
        - Возвращает количество удаленных значений
        """
        self.r.delete(*key)

    def flush(self) -> bool:
        """
        ### Метод для удаления всех ключей в текущей базе данных
        
        Out:
        - Успешность(True/False)
        """

        self.r.flushdb()

    def get_all_keys(self,
                     include_type: bool = False,
                     include_value: bool = False) -> list[dict]:
        """
        ### Метод для получения списка всех ключей и типа данных значений
        
        Args:
        - include_type  : включать значения типа
                        ['key(string):', 'key1(string):']
        - include_value : включать значение
                        ['key(string):value', 'key1(string):value']
        Out:
        - Возвращает список значений
        """

        all_keys = self.r.keys("*")
        # для каждого ключа выводим его тип данных и значение
        value = ""
        result = []

        if include_type or include_value:
            for key in all_keys:
                key_type = self.r.type(key)
                if include_value:
                    if key_type == 'string':
                        value = self.r.get(key)
                    if key_type == 'hash':
                        value = self.r.hgetall(key)
                    if key_type == 'list':
                        value = self.r.lrange(key, 0, -1)
                    if key_type == 'set':
                        value = self.r.smembers(key)
                    if key_type == 'zset':
                        value = self.r.zrange(key, 0, -1)
                    if key_type == 'stream':
                        # Последнее сообщение из потока
                        value = self.r.xread({key: '$'})
                if include_type:
                    key_type = f'({key_type})'
                else:
                    key_type = ""
                result.append(f"{key}{key_type}:{value}")
            return result
        return all_keys