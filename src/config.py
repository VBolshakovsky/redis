from os import environ
from dotenv import load_dotenv

load_dotenv()
class Settings:
    #Общие
    APP_NAME = environ.get('APP_NAME', 'redis_client')
    TIME_ZONE = environ.get('TIME_ZONE', 'Europe/Moscow')
    
    #REDIS
    REDIS_HOST = environ.get('REDIS_HOST', 'localhost')
    REDIS_PORT = environ.get('REDIS_PORT', '6379')
    REDIS_PASSWORD = environ.get('REDIS_PASSWORD', '')
    REDIS_DB = environ.get('REDIS_DB', 0)
