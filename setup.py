from setuptools import find_packages, setup

setup(
    name='redis_client',
    packages=find_packages(),
    version='0.1.0',
    description='Redis client',
    author='V.Bolshakov',
)